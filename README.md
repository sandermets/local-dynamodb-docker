# Local DynamoDB Skeleton

Start DynamoDB development/design locally. No AWS/Cloud needed.

## Ideas & Credits

    https://github.com/docker/example-voting-app   
    https://hub.docker.com/r/peopleperhour/dynamodb/~/dockerfile

## Configuration

AWS configuration and set up is via environment variables placed in docker-compose.macos.debug.yml

## Understanding and Designing DynamoDB 

*    https://d0.awsstatic.com/whitepapers/migration-best-practices-rdbms-to-dynamodb.pdf
*    http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/dynamodb-dg.pdf  
*    https://blog.runscope.com/posts/migrating-to-dynamodb-part-1-lessons-in-schema-design  
*    http://cloudacademy.com/blog/amazon-dynamodb-ten-things/  
*    https://www.nordcloud.com/tech-blog/aws-dynamodb-design-considerations  
*    http://www.slideshare.net/InfoQ/amazon-dynamodb-design-patterns-best-practices  

##  AWS JavaScript SDK

    http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/  
    http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB.html

##  AWS NodeJs/JavaScript DynamoDB getting started

    http://docs.aws.amazon.com/amazondynamodb/latest/gettingstartedguide/GettingStarted.NodeJs.html
    http://docs.aws.amazon.com/amazondynamodb/latest/gettingstartedguide/GettingStarted.JavaScript.html

## NodeJS App development

See README.md in ./nodejsapp

## Persistan data

Make directory in the hosting computer for DynamoDB to hold its data.

Copy docker-compose.macos.debug.yml or change inside the same yml file dynamodb direcotry mapping

    services:
        ...
        dynamodata:
            ...
            volumes:
                - [Absolute path]:/var/data

Read more https://docs.docker.com/engine/tutorials/dockervolumes/#/mount-a-host-directory-as-a-data-volume

## RUN docker-compose

### Run Local DynamoDB only :8000 with fresh build (preferred way for local development)

    docker-compose -f docker-compose.macos.only-dynamo.yml up --build

### Run only Local DynamoDB as deamon(service in MS Windows)

#### Start

    docker-compose -f docker-compose.macos.debug.yml --build dynamodata #update build
    docker-compose -f docker-compose.macos.debug.yml up --no-deps -d dynamodata # -d as daemon

#### Stop

    docker-compose down --no-deps -d dynamodata

### Check whether container(s) working

    docker ps

#### Run all containers

##### Start

    docker-compose -f docker-compose.macos.debug.yml up --build

##### Stop

    docker-compose -f docker-compose.macos.debug.yml down

## DynamoDB API

### Manage Tables

    CreateTable
    UpdateTable
    DeleteTable
    DescribeTable
    ListTables

### Read and Write Items

    PutItem
    GetItem
    UpdateItem
    DeleteItem

### Query specific items or Scan the full table

    Query
    Scan

### Bulk get or Update

    BatchGetItem
    BatchWriteItem

## AWS CLI

Working with AWS CLI against local dynamodb.

### Use AWS CLI

AWS credentials have to be reachable by AWS. Locally one can use fake credentials - easy way is to add credentials to ENV.  
In Mac & Ubuntu  add to .bash_profile & .bashrc respectively

    export AWS_SECRET_ACCESS_KEY=foo
    export AWS_ACCESS_KEY_ID=bar

### Install
    http://docs.aws.amazon.com/cli/latest/userguide/installing.html

### Create Table

    aws dynamodb create-table \
        --table-name TestTable \
        --attribute-definitions \
            AttributeName=Artist,AttributeType=S \
            AttributeName=SongTitle,AttributeType=S \
        --key-schema AttributeName=Artist,KeyType=HASH AttributeName=SongTitle,KeyType=RANGE \
        --provisioned-throughput ReadCapacityUnits=1,WriteCapacityUnits=1 \
        --endpoint-url http://localhost:8000

### List Tables

    aws dynamodb list-tables \
        --endpoint-url http://localhost:8000

### Put Item

    aws dynamodb put-item \
        --table-name TestTable  \
        --item \
            '{"Artist": {"S": "No One You Know"}, "SongTitle": {"S": "Call Me Today"}, "AlbumTitle": {"S": "Somewhat Famous"}}' \
        --return-consumed-capacity TOTAL \
        --endpoint-url http://localhost:8000

### Query item

    aws dynamodb query --table-name TestTable --key-conditions file://key-conditions.json

## NodeJS specific considerations

http://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/node-js-considerations.html