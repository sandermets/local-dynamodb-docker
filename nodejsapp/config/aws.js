const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY || 'foo';
const accessKeyId = process.env.AWS_ACCESS_KEY_ID || 'bar';
const dynamodbEndPoint = process.env.NODE_DYNAMO_ENDPOINT || 'http://localhost:8000';

const AWS = require('aws-sdk');
const uuid = require('uuid');
const bluebird = require('bluebird');

AWS.config.update({
  region: 'us-west-1',
  endpoint: dynamodbEndPoint
});

AWS.config.setPromisesDependency(bluebird);

module.exports = AWS;