const express = require('express');
const apiRoutes = express.Router();
const authService = require('../services/authentication');
const initTablesController = require('../controllers/create-tables');
const controllerTableTest = require('../controllers/table-test');

apiRoutes.route('/init-tables')
  .get(authService.publicAccess, initTablesController.getCreateTables);

apiRoutes.route('/table-test')
  .get(authService.publicAccess, controllerTableTest.getTestTables)
  .post(authService.publicAccess, controllerTableTest.postTestTable)
  .put(authService.publicAccess, controllerTableTest.putTestTable)
  .delete(authService.publicAccess, controllerTableTest.deleteTestTable);

// /table-test/:year/:title vs /table-test/year/:year/title/:title
// so title can be unicode or whatever, so let's create simple put
// apiRoutes.route('/table-test/:year/:title')
// .put(authService.publicAccess, controllerTableTest.putTestTable);

module.exports = apiRoutes;