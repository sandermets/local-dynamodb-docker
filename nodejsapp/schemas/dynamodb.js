var dynamodb = require('../adapter/dynamodb');

function createTestTable() {

  let tableName = 'TestTable';

  let params = {
    TableName: tableName,
    KeySchema: [
      { AttributeName: "year", KeyType: "HASH" },  //Partition key
      { AttributeName: "title", KeyType: "RANGE" }  //Sort key
    ],
    AttributeDefinitions: [
      { AttributeName: "year", AttributeType: "N" },
      { AttributeName: "title", AttributeType: "S" }
    ],
    ProvisionedThroughput: {
      ReadCapacityUnits: 1,
      WriteCapacityUnits: 1
    }
  };

  dynamodb.createTable(params).promise()
    .then(data => {
      console.log(tableName, "Created table. Table description JSON:", JSON.stringify(data, null, 2));
    })
    .catch(err => {
      console.error(tableName, "Unable to create table. Error JSON:", JSON.stringify(err, null, 2));
    });
}

exports.createTables = () => {
  createTestTable();
}