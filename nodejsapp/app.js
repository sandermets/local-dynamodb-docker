const port = process.env.NODE_PORT || 3333;

const express = require('express');
const app = express();
const apiRoutes = require('./router/api-routes');
const Promise = require('bluebird');
const bodyParser = require('body-parser');//accept data via POST or PUT

app.use(bodyParser.json());

app.use('/api', apiRoutes);

app.listen(port);//boot node up
console.log(`Successfully started! Listening on :${port}`);

process.on('SIGINT', () => {
  //handle shutting down resources gracefully
  console.log('SIGINT caught');
});