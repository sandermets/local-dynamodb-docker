const schemaDynamoDB = require('../schemas/dynamodb');

/**
 * @api {get} /api/init-tables
 */
exports.getCreateTables = (req, res, next) => {
  schemaDynamoDB.createTables();
  res.json({ success: true, code: 200, message: 'Creating tables in progress...' });
}