const serviceTableTest = require('../services/table-test');

/**
 * Query table by url params.
 * If no url params then scan? 
 * @api {get} /api/test-table?year=2017
 */
exports.getTestTables = (req, res, next) => {
  if (Object.keys(req.query).length) {
    if (!req.query.year) {
      return res.json({ success: false, code: 400, message: 'Invalid' });
    }
    serviceTableTest.query(req.query)
      .then(data => {
        res.json({ success: true, code: 200, length: data.Items.length, data: data });
        console.log(`getTestTable ${data}`);
      })
      .catch(err => {
        res.json({ success: false, code: 400, message: err.message });
        console.warn(`getTestTable ${err}`);
      });
  } else {
    serviceTableTest.scan(req.query)
      .then(data => {
        res.json({ success: true, code: 200, length: data.Items.length, data: data });
        console.log(`getTestTable ${data}`);
      })
      .catch(err => {
        res.json({ success: false, code: 400, message: err.message });
        console.warn(`getTestTable ${err}`);
      });
  }
};

/**
 * @api {post} /api/table-test
 */
exports.postTestTable = (req, res, next) => {

  if (!req.body.year || !req.body.title) {
    return res.json({ success: false, code: 400, message: 'Invalid' });
  }

  req.body.year = Number(req.body.year);

  serviceTableTest.put(req.body)
    .then(data => {
      res.json({ success: true, code: 202, data: data });
      console.log(`postTestTable ${data}`);
    })
    .catch(err => {
      res.json({ success: false, code: 400, message: err.message });
      console.warn(`postTestTable ${err}`);
    });
};

/**
 * @api {put} /api/test-table
 */
exports.putTestTable = (req, res, next) => {

  //what if only year is given? will it update several docs?
  if (!req.body.year || !req.body.title) {
    return res.json({ success: false, code: 400, message: 'Invalid' });
  }

  req.body.year = Number(req.body.year);

  serviceTableTest.update(req.body)
    .then(result => {
      res.json({ success: true, code: 202, data: result });
      console.log(result);
    })
    .catch(err => {
      res.json({ success: false, code: 400, message: err.message });
      console.warn(err);
    });
};

/**
 * @api {delete} /api/test-table
 */
exports.deleteTestTable = (req, res, next) => {

  //what if only year is given? will it update several docs?
  if (!req.body.year || !req.body.title) {
    return res.json({ success: false, code: 400, message: 'Invalid' });
  }

  req.body.year = Number(req.body.year);

  serviceTableTest.delete(req.body)
    .then(result => {
      res.json({ success: true, code: 202, data: result });
      console.log(result);
    })
    .catch(err => {
      res.json({ success: false, code: 400, message: err.message });
      console.warn(err);
    });
};