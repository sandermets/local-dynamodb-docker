/**
 * HELP :: http://docs.aws.amazon.com/amazondynamodb/latest/gettingstartedguide/GettingStarted.NodeJs.03.html
 */
const docClient = require('../adapter/doc-client');
const tableName = 'TestTable';

/**
 * Has to have queryParams
 */
exports.query = (queryParams) => {
  return docClient
    .query({
      TableName: tableName,
      KeyConditionExpression: "#yr = :yyyy",
      ExpressionAttributeNames: {
        "#yr": "year"
      },
      ExpressionAttributeValues: {
        ":yyyy": Number(queryParams.year)
      }
    })
    .promise();
};

/**
 * Careful here
 * The scan method reads every item in the entire table, 
 * and returns all of the data in the table. 
 * You can provide an optional filter_expression, 
 * so that only the items matching your criteria are returned. 
 * However, note that the filter is only applied after 
 * the entire table has been scanned.
 */
exports.scan = (req, res, next) => {
  return docClient
    .scan({
      TableName: tableName
    })
    .promise();
};

/**
 * Create new row
 */
exports.put = (reqBody) => {
  return docClient
    .put({
      TableName: tableName,
      Item: reqBody
    })
    .promise();
};

/**
 * Update
 */
exports.update = (reqBody) => {

  return docClient
    .update({
      TableName: tableName,
      Key: {
        "year": reqBody.year,
        "title": reqBody.title
      },
      UpdateExpression: "set info.rating = :r, info.plot=:p, info.actors=:a",
      ExpressionAttributeValues: {
        ":r": 5.5,
        ":p": "Everything happens all at once.",
        ":a": ["Larry", "Moe", "Curly"]
      },
      ReturnValues: "UPDATED_NEW"
    })
    .promise();
};

exports.delete = (reqBody) => {
  return docClient
    .delete({
      TableName: tableName,
      Key: {
        "year": reqBody.year,
        "title": reqBody.title
      }
    })
    .promise();
};
