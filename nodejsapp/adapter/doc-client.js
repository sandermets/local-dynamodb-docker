var AWS = require('../config/aws');
var docClient;

if (!docClient) {
  console.log('new AWS.DynamoDB.DocumentClient()');
  docClient = new AWS.DynamoDB.DocumentClient();
}

module.exports = docClient;