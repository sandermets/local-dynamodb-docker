var AWS = require('../config/aws');
var dynamodb;

if (!dynamodb) {
  console.log('new AWS.DynamoDB()');
  dynamodb = new AWS.DynamoDB();
}

module.exports = dynamodb;