# NodeJS app

## Install

    ./nodejsapp $ npm i  

## Start Local DynamoDB

    ./ $ docker-compose -f docker-compose.macos.only-dynamo.yml up --build

Success if similar output:  

  ```
  dynamodata_1  | Initializing DynamoDB Local with the following configuration:
  dynamodata_1  | Port:	8000
  dynamodata_1  | InMemory:	false
  dynamodata_1  | DbPath:	/var/data
  dynamodata_1  | SharedDb:	true
  dynamodata_1  | shouldDelayTransientStatuses:	true
  dynamodata_1  | CorsParams:	*
  ```

Docker container can also be run with docker itself without docker-compose, though mappings
should be done in terminal.

## Run/Debug

Use chrome-devtools for debugging:  

    ./nodejsapp $ node --inspect=5858 app.js

Use IDE, follow instructions from VSCode or other IDE.

## Create table(s)

In Browser/Curl/Postman GET request:  

    http://localhost:3333/api/init-tables

## Postman collection

TODO





